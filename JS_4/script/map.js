
function map(fn, array) {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        newArray[i] = fn(array[i]);
    }
    return newArray;
}

const myArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

const exp = a => {
    return a**3;
}

map(a => { return a**3 }, myArray)

document.getElementById("res-map").innerHTML = map(exp, myArray);
    
    //`[${map(square, myArray).join(', ')}]`;
    